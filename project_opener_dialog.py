# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ProjectOpenerDialog
                                 A QGIS plugin
 A simple plugin to display a list of projects in a default location to the user, so it's possible to select it and open it with QGis.
                             -------------------
        begin                : 2015-01-14
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Sigma Geosistemas LTDA - www.sigmageosistemas.com.br
        email                : george@consultoriasigma.com.br
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os
import ConfigParser
from PyQt4 import QtGui, uic
from PyQt4.QtCore import QSettings, SIGNAL
from PyQt4.QtGui import QListWidgetItem, QSizePolicy, QDialogButtonBox, QGridLayout
from qgis.gui import QgsMessageBar


FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'project_opener_dialog_base.ui'))
DEFAULT_PROJECT_PATH = "~/Desktop"

class ProjectOpenerDialog(QtGui.QDialog, FORM_CLASS):

    project_path = None

    def __init__(self, parent=None, iface=None):
        """Constructor."""
        super(ProjectOpenerDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.iface = iface
        self.setupUi(self)
        self._layout()
        self.load_settings()
        self.load_project_path_text_box()
        self.load_projects()
        self.hook_signals()


    def _layout(self):

        self.bar = QgsMessageBar()
        self.bar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.layoutVertical.addWidget(self.bar)
        self.layoutVertical.addWidget(self.tabManager)

    def hook_signals(self):

        self.connect(self.project_list_widget, SIGNAL("itemDoubleClicked (QListWidgetItem *)"), self.open_project)

    def load_projects(self):

        self.tabManager.setCurrentIndex(0)
        self.project_list_widget.clear()

        if os.path.exists(self.project_path):

            onlyfiles = [f for f in os.listdir(self.project_path) if os.path.isfile(os.path.join(self.project_path,f)) if os.path.basename(f).endswith(".qgs")]

            if len(onlyfiles) <= 0:
                self.bar.pushMessage("Empty folder", "Please change the project folder.", QgsMessageBar.WARNING)
                self.tabManager.setCurrentIndex(1)

            for f in onlyfiles:
                itemName = u"{0}".format(os.path.join(self.project_path, f))
                self.project_list_widget.addItem(itemName)
        else:
            self.bar.pushMessage("Project folder does not exist", "Please change the project folder.", QgsMessageBar.WARNING)
            self.tabManager.setCurrentIndex(1)

    def load_settings(self):

        self.settings = QSettings()
        self.project_path = unicode(self.settings.value("project_opener/project_path", DEFAULT_PROJECT_PATH))
        self.project_path = os.path.expanduser(self.project_path)

    def store_settings(self):

        new_text = u"%s" % self.project_path_textbox.text()

        if self.settings.value("project_opener/project_path") != new_text:
            self.settings.setValue("project_opener/project_path", new_text)

    def load_project_path_text_box(self):

        self.project_path_textbox.setText(self.project_path)

    def open_project(self, item):

        result = self.iface.addProject(item.text())
        if result:
            self.accept()






