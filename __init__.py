# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ProjectOpener
                                 A QGIS plugin
 A simple plugin to display a list of projects in a default location to the user, so it's possible to select it and open it with QGis.
                             -------------------
        begin                : 2015-01-14
        copyright            : (C) 2015 by Sigma Geosistemas LTDA - www.sigmageosistemas.com.br
        email                : george@consultoriasigma.com.br
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load ProjectOpener class from file ProjectOpener.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .project_opener import ProjectOpener
    return ProjectOpener(iface)
