# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=ProjectOpener
qgisMinimumVersion=2.0
description=A simple plugin to display a list of projects in a default location to the user, so it's possible to select it and open it with QGis.
version=0.1
author=Sigma Geosistemas LTDA - www.sigmageosistemas.com.br
email=george@consultoriasigma.com.br

# End of mandatory metadata

# Optional items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=proprietary, custom-developement, helper

homepage=www.sigmageosistemas.com.br
tracker=https://github.com/sigma-geosistemas/project-opener.git
repository=https://github.com/sigma-geosistemas/project-opener.git
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

